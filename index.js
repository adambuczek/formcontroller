/**
 * Form controller. Changes normal form submit into AJAX request
 *
 * Gather form inputs values
 *
 */
'use strict';

var $ = require('jquery');

/**
* options {
    validator: function used to validate form on submit, must take the form jQ obj as only argument and return true or false
  }
*/
var FormController = function(form, options) {

    var defaults = {
        validator: false,
        success: function (){ return undefined; }, /* must be callable */
        error: function (){ return undefined; }, /* must be callable */
        alwaysAfter: function (){ return undefined; }, /* must be callable */
        beforeSend: function (){ return undefined; }, /* must be callable */
        submitEl: form.find('[type="submit"]'),
        progressIndicatorEl: form,
        progressIndicatorClasses: {
          loading: 'js--loading',
          done: 'js--done',
          success: 'js--success',
          error: 'js--error'
        }
    };

    this.form = form;
    this.options = $.extend({}, defaults, options);

    this.success = this.options.success.bind(this);
    this.error = this.options.error.bind(this);
    this.alwaysAfter = this.options.alwaysAfter.bind(this);
    this.beforeSend = this.options.beforeSend.bind(this);

};

FormController.prototype.validate = function() {
    return this.options.validator(this.form);
};

FormController.prototype.reset = function() {
    // remove all classes that could have been added to form
    var cls = this.options.progressIndicatorClasses;
    this.options.progressIndicatorEl.removeClass(Object.keys(cls).map(function(cl){return cls[cl]}).join(' '));
    this.options.submitEl.prop("disabled",false);
};

FormController.prototype.init = function() {

    var that = this;

    // console.log('form init');

    // disable HTML5 validation
    this.form.attr('novalidate', 'novalidate');

    this.form.on('submit', function(e) {


        var progIndicEl = that.options.progressIndicatorEl,
            progIndicCls = that.options.progressIndicatorClasses,
            formData = new FormData(this);

        e.preventDefault();

        //// Validate
        if (!that.options.validator || that.validate()) {

            that.beforeSend();

            // SEND
            progIndicEl.removeClass(progIndicCls.done)
              .removeClass(progIndicCls.error)
              .removeClass(progIndicCls.success)
              .addClass(progIndicCls.loading);

            $.ajax({
                url: $(this).attr('action'),
                type: $(this).attr('method'),
                data: $(this).serialize()
            }).done(function() {
                progIndicEl
                  .removeClass(progIndicCls.loading)
                  .addClass(progIndicCls.success);
                that.success();
            }).fail(function() {
                progIndicEl
                  .removeClass(progIndicCls.loading)
                  .addClass(progIndicCls.error);
                that.error();
            }).always(function() {
                progIndicEl.addClass(progIndicCls.done);
                that.alwaysAfter();
            });

        }

        return false;

    });

};

module.exports = FormController;
